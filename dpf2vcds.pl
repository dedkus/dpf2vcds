#!perl
#convert output log  from Android VAG DPF aplication to format which is used by other diagnostic software
#so existing graphing can be used
#dedkus 2019
use strict;
use warnings;
use utf8;
use POSIX qw( strftime );
use Time::Local; #timelocal
use POSIX qw(strftime locale_h); #LC_TIME
use locale;

system('chcp 65001'); #:-( yes we expect windows, sorry
binmode(STDOUT,":utf8"); #but do not like non unicode
binmode(STDERR,":utf8");
my $if="Vag_DPF_log.txt";
$if=$ARGV[0] if defined $ARGV[0]; 
print "in $if\n";
my $of=$if;
$of=~s/(.*)\..*$/$1\.csv/; #replace .ext with .csv
$of=$of.".out" if $of eq $if;
print "out $of\n";
open(I,"<:encoding(UTF-8)",$if) or die "open fi $!";
unlink $of;
open(O,">:encoding(latin1)",$of) or die "open fo $!";

my $rows=0;
my @cols=qw (1 1 1 1 1 1 1 1 1 1 1 1 1); #cols I like to se in output
eval{
   my $lasttime;
   my @coln;   #column names
   my $datacols;
   my $timeds; #delta seconds since start
   while(<I>){
      #die "test end\n" if $rows>100;
      $rows++;
      if($rows==1){
         #todo: check format version
         #todo: diferent versions maybe differ
         #zatim texty natvrdo
         my $formatcheck=1;
         my $timef;
         my $timeh;  #timefrom header
         my $version;
         my $temp=$_;
         if($temp=~/Save date\(yyyy-MM-dd HH:mm:ss\): ([0-9\- \:]+)/){
            $timeh=$1;
            my ($y,$m,$d,$H,$M,$S)   = $timeh =~ m!(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2}):(\d{2})!;
            my $time = timelocal( $S, $M, $H, $d, $m, $y-1900 );
            #perl -e "use POSIX; print setlocale(LC_TIME)"
            #Czech_Czechia.1250
            setlocale(LC_TIME, "English_United States.1252") or die "Pardon";
            $timef=strftime('%A,%d,%B,%Y,%H:%M:%S', $S,$M,$H,$d,$m,$y-1900);
         }else{
            die "input file content is not fine: time check expected string Save date(yyyy-MM-dd HH:mm:ss)\n";
         }
         if($temp=~/Application Release: ([A-Za-z \.0-9]+)/){
            $version=$1;
         }else{
            die "input file content is not fine: version check expected string Application Release:\n";
         }
         print O "$timef:16348-VCID:2F3AE382B79EB1BFD8D-807A, FAKE Version: Release $version(x64),Data version: 20180518 DS287.1\n";
         printf O "03L 907 309 H,ADVMB,%s,\n",$timeh; #ECU type,?,? I put original formated date
      }elsif($rows==2){
         print O ",,";
         for(my $i=0;$i<scalar @cols;$i++){
            printf O "G%03i,F0,",$i;
         }
         print O "\n\n";
         print O "Marker,";
         for(my $i=0;$i<scalar @cols;$i++){
            if($cols[$i]==1){
               printf O "TIME,Loc. IDE%05i,",$i;
            }
         }
         print O "\n";
         print O ",";
         @coln=split(/\t/,$_);
         $datacols=scalar @coln;
         my $timen=shift @coln;
         my $units;
         for(my $i=0;$i<scalar @cols;$i++){
            if($cols[$i]==1){
               (my $t,my $u);
         if($coln[$i]=~/([A-Za-z0-9\s\.]*)\((.*)\)/){
            $t=$1;$u=$2;
            chop $t;
         }
               printf O "STAMP,%s,",$t;
         $units.=",$u,";
            }
         }
         print O "\n";
         print O ",$units\n";
      }else{
         next if $_=~/Save date/;
         next if $_=~/Time/;
         next if $_=~/^\s*$/;   
         my @colv=split(/\t/,$_);
         if($datacols!=scalar @colv){
            printf STDERR "unexpected number of values expecting %i, get %i, skipping row\n",$datacols,scalar @colv;
            next;
         }
         my $timev=shift @colv;
         my $timef=strftime("%Y-%m-%d %H:%M:%S", localtime($timev/1000));
         $timef.=sprintf ".%s",$timev%1000;
         if(not defined $lasttime){
            printf "1st data from %s\n",$timef;
            $lasttime=$timev  
         };
         my $marker='';
         if ($timev-$lasttime>100000){
            $marker=1;
         };
         $timeds+=($timev-$lasttime)/1000; 
         my $timedsf=sprintf "%.3f",$timeds;
         $lasttime=$timev;
         my $r="$marker,";
         my $next=0;
         for(my $i=0;$i<scalar @cols;$i++){
            if($colv[$i]=~/^\s*$/){
               printf STDERR "%s %.3f empty value %s, skipping row\n",$timef,$timedsf,$coln[$i];
               $next=1;
               next;
            };
            if($cols[$i]==1){
               $r.=sprintf "%s,%s,",$timedsf,$colv[$i];
            }
         }
         next if $next>0;
         print O "$r\n" ;
         #print "$rows $r";

      };
   };
};
  print "end\n";
if($@){
	print STDOUT $@;
};
close I;
close O;
